/*
 * Copyright (c) 2021 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

/*
 * These are the two requests we send to the TR-064 interface for the status of
 * of the DSL link and IP connection, respectively.
 * XXX: hard-wires the host name, port number, and User-Agent version.
 */
uint8_t dsl_status_request[] =
  "POST /igdupnp/control/WANCommonIFC1 HTTP/1.1\r\n"
  "Host: fritz.box:49000\r\n"
  "User-Agent: pico-fritzmon/trunk\r\n"
  "Accept: text/xml\r\n"
  "Content-Type: text/xml\r\n"
  "soapaction: urn:schemas-upnp-org:service:WANCommonInterfaceConfig:1#GetCommonLinkProperties\r\n"
  "Content-Length: 380\r\n"
  "\r\n"
  "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
  "        <s:Envelope s:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\"\n"
  "                    xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\"><s:Body>\n"
  "        <u:GetCommonLinkProperties xmlns:u=\"urn:schemas-upnp-org:service:WANCommonInterfaceConfig:1\">\n"
  "        </u:GetCommonLinkProperties>\n"
  "        </s:Body>\n"
  "        </s:Envelope>\n\n",

  ip_status_request[] =
  "POST /igdupnp/control/WANIPConn1 HTTP/1.1\r\n"
  "Host: fritz.box:49000\r\n"
  "User-Agent: pico-fritzmon/trunk\r\n"
  "Accept: text/xml\r\n"
  "Content-Type: text/xml\r\n"
  "soapaction: urn:schemas-upnp-org:service:WANIPConnection:1#GetStatusInfo\r\n"
  "Content-Length: 351\r\n"
  "\r\n"
  "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
  "        <s:Envelope s:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\"\n"
  "                    xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\"><s:Body>\n"
  "        <u:GetStatusInfo xmlns:u=\"urn:schemas-upnp-org:service:WANIPConnection:1\">\n"
  "        </u:GetStatusInfo>\n"
  "        </s:Body>\n"
  "        </s:Envelope>\n\n";
