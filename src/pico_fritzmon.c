/*
 * Copyright (c) 2021 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "pico/stdlib.h"
#include "pico/binary_info.h"
#include "pico/sleep.h"
#include "pico/bootrom.h"
#include "pico/util/datetime.h"
#include "hardware/clocks.h"
#include "hardware/rosc.h"
#include "hardware/sync.h"
#include "hardware/structs/scb.h"
#include "hardware/exception.h"

#include <rp2040-esp-at.h>

#include "status_requests.h"
#include "creds.h"

#define CHECK_INTERVAL_SECS (15)
#define CHECK_INTERVAL_MS (secs2ms(CHECK_INTERVAL_SECS))
#define TIMEOUT (secs2ms(5))
#define LED_ON_MS (50)
#define RX_BUFLEN (1024)

static bool wifi_good, dsl_good, ip_good;
static volatile bool awake;

static struct esp_at_dhcp dhcp =
  {
	.station = 1,
	.softap = 0,
	.enable = 1,
  };

static struct esp_at_ap ap =
  {
	.bssid[0] = '\0',
	.pci_en = -1,
  };

/* WiFi creds included from creds.h */
static char *password = PASSWD;

/* XXX hostname and port number currently hard-wired */
static struct esp_at_cx cx =
  {
   .addr = "fritz.box",
   .port = 49000,
   .type = ESP_AT_CX_TYPE_TCP,
   .keepalive = 7200,
   .linkId = -1,
  };

static datetime_t epoch =
{
	.year  = 1970,
	.month = 1,
	.day   = 1,
	.dotw  = 4,
	.hour  = 0,
	.min   = 0,
	.sec   = 0,
};

uint8_t rxbuf[RX_BUFLEN];

static inline uint32_t
secs2ms(int secs)
{
	return (secs * 1000);
}

static inline uint32_t
retry_sleep(uint32_t retry_intvl)
{
	sleep_ms(retry_intvl);
	if (retry_intvl < 1000)
		retry_intvl <<= 1;
	return (retry_intvl);
}

#define HTTP_RESP_PFX_LEN (sizeof("HTTP/x.x 999 ")-1)

static uint16_t
get_http_status(uint8_t *buf, int len)
{
	uint16_t status = 0;
	uint8_t *p;

	if (len < HTTP_RESP_PFX_LEN)
		return (500);
	if (strncmp(buf, "HTTP/", sizeof("HTTP/")-1) != 0)
		return (500);
	if (!isdigit(buf[5]))
		return (500);
	p = memchr(buf, ' ', HTTP_RESP_PFX_LEN);
	if (p == NULL)
		return (500);
	p++;
	if (p[3] != ' ')
		return (500);
	for (int i = 0; i < 3; i++) {
		if (!isdigit(p[i]))
			return (500);
		status *= 10;
		status += p[i] - '0';
	}
	return (status);
}

#define DSL_XML_TAG "NewPhysicalLinkStatus"

static bool
check_dsl_status(uint8_t *buf, int len)
{
	uint8_t *p = buf, *b;
	
	for (int l = len; l > sizeof(DSL_XML_TAG) + 1; l = (len - (p - buf))) {
		p = memchr(p, '<', l);
		if (p == NULL)
			return (false);
		p++;
		if (strncmp(p, DSL_XML_TAG ">", sizeof(DSL_XML_TAG)) != 0)
			continue;
		b = p + sizeof(DSL_XML_TAG);
		l = len - (b - buf);
		p = memchr(b, '<', l);
		if (p == NULL)
			return (false);
		if (strncmp(p + 1, "/" DSL_XML_TAG ">", sizeof(DSL_XML_TAG)+1)
		    != 0)
			return (false);
		return (strncmp(b, "Up", 2) == 0);
	}
	return (false);
}

#define IP_XML_TAG "NewConnectionStatus"

static bool
check_ip_status(uint8_t *buf, int len)
{
	uint8_t *p = buf, *b;
	
	for (int l = len; l > sizeof(IP_XML_TAG) + 1; l = (len - (p - buf))) {
		p = memchr(p, '<', l);
		if (p == NULL)
			return (false);
		p++;
		if (strncmp(p, IP_XML_TAG ">", sizeof(IP_XML_TAG)) != 0)
			continue;
		b = p + sizeof(IP_XML_TAG);
		l = len - (b - buf);
		p = memchr(b, '<', l);
		if (p == NULL)
			return (false);
		if (strncmp(p + 1, "/" IP_XML_TAG ">", sizeof(IP_XML_TAG)+1)
		    != 0)
			return (false);
		return (strncmp(b, "Connected", 9) == 0);
	}
	return (false);
}

static int
connect_ap()
{
	esp_at_err_t err;
	int cx_status, ip;

	err = esp_at_wifi_set_mode_timeout_ms(esp_at_hndl0,
					      ESP_AT_WIFI_MODE_STATION,
					      TIMEOUT);
	if (err != ESP_AT_ERR_OK)
		return (-1);

	err = esp_at_wifi_set_dhcp_timeout_ms(esp_at_hndl0, &dhcp, TIMEOUT);
	if (err != ESP_AT_ERR_OK)
		return (-1);

	/*
	 * AT+CWJAP_CUR too rapidly after setting DHCP has caused the
	 * ESP8266 to reset.
	 */
	sleep_ms(1000);

	err = esp_at_wifi_set_ap_timeout_ms(esp_at_hndl0, &ap, password,
					    &cx_status, &ip, TIMEOUT);
	if (err != ESP_AT_ERR_OK)
		return (-1);
	if (!cx_status)
		return (-1);
	if (!ip)
		return (-1);

	err = esp_at_tcp_set_recvmode_timeout_ms(esp_at_hndl0,
						 ESP_AT_RECV_MODE_PASSIVE,
						 TIMEOUT);
	if (err != ESP_AT_ERR_OK)
		return (-1);

	return (0);
}

static int
connect_fritz()
{
	esp_at_err_t err;

	err = esp_at_tcp_connect_timeout_ms(esp_at_hndl0, &cx, TIMEOUT);
	if (err != ESP_AT_ERR_OK)
		return (-1);
	return (0);
}

static void
status_check()
{
	struct esp_at_status status;
	char ip[ESP_AT_IPV4_MAXLEN];
	esp_at_err_t err;
	int resp_len = 0;
	uint16_t response_status;
	uint32_t retry_intvl = 1;

	do {
		err = esp_at_base_test_timeout_ms(esp_at_hndl0, TIMEOUT);
		if (err != ESP_AT_ERR_OK) {
			retry_intvl = retry_sleep(retry_intvl);
			if (retry_intvl > 2000)
				return;
		}
	} while (err != ESP_AT_ERR_OK);

	status.remote_ip = ip;
	err = esp_at_tcp_get_status_timeout_ms(esp_at_hndl0, &status,
					       TIMEOUT);
	if (err != ESP_AT_ERR_OK)
		return;
	if (status.status != ESP_AT_STATUS_CX) {
		if (status.status != ESP_AT_STATUS_AP)
			if (connect_ap() != 0)
				return;
		if (connect_fritz() != 0)
			return;
	}
	wifi_good = true;

	err = esp_at_tcp_set_recvmode_timeout_ms(esp_at_hndl0,
						 ESP_AT_RECV_MODE_PASSIVE,
						 TIMEOUT);
	if (err != ESP_AT_ERR_OK)
		return;

	err = esp_at_tcp_send_timeout_ms(esp_at_hndl0, -1, dsl_status_request,
					 sizeof(dsl_status_request)-1, TIMEOUT);
	if (err != ESP_AT_ERR_OK)
		return;

	while (resp_len == 0) {
		err = esp_at_tcp_recv_timeout_ms(esp_at_hndl0, -1, rxbuf,
						 RX_BUFLEN, &resp_len, TIMEOUT);
		if (err != ESP_AT_ERR_OK)
			return;
	}
	if (resp_len > RX_BUFLEN)
		return;
	response_status = get_http_status(rxbuf, resp_len);
	if (response_status != 200)
		return;
	if (check_dsl_status(rxbuf, resp_len))
		dsl_good = true;
	else
		return;

	err = esp_at_tcp_send_timeout_ms(esp_at_hndl0, -1, ip_status_request,
					 sizeof(ip_status_request)-1, TIMEOUT);
	if (err != ESP_AT_ERR_OK)
		return;

	resp_len = 0;
	while (resp_len == 0) {
		err = esp_at_tcp_recv_timeout_ms(esp_at_hndl0, -1, rxbuf,
						 RX_BUFLEN, &resp_len, TIMEOUT);
		if (err != ESP_AT_ERR_OK)
			return;
	}
	if (resp_len > RX_BUFLEN)
		return;
	response_status = get_http_status(rxbuf, resp_len);
	if (response_status != 200)
		return;
	if (check_ip_status(rxbuf, resp_len))
		ip_good = true;
}

static void
set_deadline(datetime_t *t, int secs)
{
	datetime_t now;
	struct tm tm, *tmd;
	time_t time;

	rtc_get_datetime(&now);

	tm.tm_year = now.year - 1900;
	tm.tm_mon  = now.month - 1;
	tm.tm_mday = now.day;
	tm.tm_wday = now.dotw;
	tm.tm_hour = now.hour;
	tm.tm_min  = now.min;
	tm.tm_sec  = now.sec;

	time = mktime(&tm) + secs;
	tmd = gmtime(&time);

	t->year  = tmd->tm_year + 1900;
	t->month = tmd->tm_mon + 1;
	t->day   = tmd->tm_mday;
	t->dotw  = tmd->tm_wday;
	t->hour  = tmd->tm_hour;
	t->min   = tmd->tm_min;
	t->sec   = tmd->tm_sec;
}

static void
on_wakeup(void)
{
	awake = true;
}

static void
on_hardfault(void)
{
	reset_usb_boot(0, 0);
}

int
main(void)
{
	esp_at_err_t err;
	uint32_t retry_intvl = 1;

	const uint ONBOARD_LED = 25, WIFI_GREEN = 15, WIFI_RED = 14,
		DSL_GREEN = 13, DSL_RED = 12, IP_GREEN = 11, IP_RED = 10;
	uint32_t led_mask = (1 << WIFI_GREEN) | (1 << WIFI_RED)
		| (1 << DSL_GREEN) | (1 << DSL_RED)
		| (1 << IP_GREEN) | (1 << IP_RED), status_mask;

	exception_set_exclusive_handler(HARDFAULT_EXCEPTION, on_hardfault);

	uart_init(PICO_DEFAULT_UART_INSTANCE, PICO_DEFAULT_UART_BAUD_RATE);
	gpio_set_function(PICO_DEFAULT_UART_TX_PIN, GPIO_FUNC_UART);
	gpio_set_function(PICO_DEFAULT_UART_RX_PIN, GPIO_FUNC_UART);
	gpio_pull_up(PICO_DEFAULT_UART_RX_PIN);

	/* For picotool info */
	bi_decl(bi_2pins_with_names(WIFI_GREEN, "WIFI GOOD",
				    WIFI_RED, "WIFI DOWN"));
	bi_decl(bi_2pins_with_names(DSL_GREEN, "DSL GOOD",
				    DSL_RED, "DSL DOWN"));
	bi_decl(bi_2pins_with_names(IP_GREEN, "IP GOOD", IP_RED, "IP DOWN"));

	gpio_init_mask((1 << ONBOARD_LED) | led_mask);
	gpio_set_dir_out_masked((1 << ONBOARD_LED) | led_mask);

	rtc_init();
	rtc_set_datetime(&epoch);

	do {
		err = esp_at_hndl_init(esp_at_hndl0);
		if (err != ESP_AT_ERR_OK)
			retry_intvl = retry_sleep(retry_intvl);
	} while (err != ESP_AT_ERR_OK);

	retry_intvl = 1;
	do {
		err = esp_at_base_reset_timeout_ms(esp_at_hndl0, NULL, 0,
						   TIMEOUT);
		if (err != ESP_AT_ERR_OK)
			retry_intvl = retry_sleep(retry_intvl);
	} while (err != ESP_AT_ERR_OK);
	/* Sleeping one second after AT+RST appears to be necessary. */
	sleep_ms(1000);

	retry_intvl = 1;
	do {
		err = esp_at_base_test_timeout_ms(esp_at_hndl0, TIMEOUT);
		if (err != ESP_AT_ERR_OK)
			retry_intvl = retry_sleep(retry_intvl);
	} while (err != ESP_AT_ERR_OK);

	retry_intvl = 1;
	do {
		err = esp_at_base_set_echo_timeout_ms(esp_at_hndl0, 0, TIMEOUT);
		if (err != ESP_AT_ERR_OK)
			retry_intvl = retry_sleep(retry_intvl);
	} while (err != ESP_AT_ERR_OK);

	strcpy(ap.ssid, SSID);

	for (;;) {
		uint sleep_en0_save, sleep_en1_save, scr_save, uart_baud;
		datetime_t deadline;

		gpio_put(ONBOARD_LED, true);
		sleep_ms(LED_ON_MS);
		gpio_put(ONBOARD_LED, false);

		wifi_good = dsl_good = ip_good = false;
		(void)status_check();
		status_mask = 0;
		if (wifi_good)
			status_mask |= (1 << WIFI_GREEN);
		else
			status_mask |= (1 << WIFI_RED);
		if (dsl_good)
			status_mask |= (1 << DSL_GREEN);
		else
			status_mask |= (1 << DSL_RED);
		if (ip_good)
			status_mask |= (1 << IP_GREEN);
		else
			status_mask |= (1 << IP_RED);
		gpio_put_masked(led_mask, status_mask);
		sleep_ms(LED_ON_MS);
		gpio_put_masked(led_mask, 0);

		(void)esp_at_base_deep_sleep_timeout_ms(
			esp_at_hndl0, CHECK_INTERVAL_MS - secs2ms(1), TIMEOUT);

		set_deadline(&deadline, CHECK_INTERVAL_SECS);

		awake = false;
		sleep_run_from_xosc();

		// Save the clock register values to restore on wakeup
		sleep_en0_save = clocks_hw->sleep_en0;
		sleep_en1_save = clocks_hw->sleep_en1;
		scr_save = scb_hw->scr;

		// Turn off all clocks when in sleep mode except for RTC
		clocks_hw->sleep_en0 = CLOCKS_SLEEP_EN0_CLK_RTC_RTC_BITS;
		clocks_hw->sleep_en1 = 0x0;

		rtc_set_alarm(&deadline, on_wakeup);

		// Enable deep sleep at the proc
		scb_hw->scr = scr_save | M0PLUS_SCR_SLEEPDEEP_BITS;

		do {
			__wfi();
		} while (!awake);

		rosc_write(&rosc_hw->ctrl, ROSC_CTRL_ENABLE_BITS);

		scb_hw->scr = scr_save;
		clocks_hw->sleep_en0 = sleep_en0_save;
		clocks_hw->sleep_en1 = sleep_en1_save;

		clocks_init();

		uart_init(PICO_DEFAULT_UART_INSTANCE,
			  PICO_DEFAULT_UART_BAUD_RATE);
	}
}
