cmake_minimum_required(VERSION 3.12)

# Pull in SDK (must be before project)
include(pico_sdk_import.cmake)

# Pull in pico-extras
include(pico_extras_import.cmake)

project(pico_fritzmon C CXX ASM)
set(CMAKE_C_STANDARD 11)
set(CMAKE_CXX_STANDARD 17)

# Initialize the SDK
pico_sdk_init()

add_subdirectory(lib/libat-esp8266 rp2040-esp-at)

add_executable(pico_fritzmon
  ${CMAKE_CURRENT_LIST_DIR}/src/pico_fritzmon.c
  ${CMAKE_CURRENT_LIST_DIR}/src/creds.h
  ${CMAKE_CURRENT_LIST_DIR}/src/status_requests.h
)

target_include_directories(pico_fritzmon INTERFACE
  lib/libat-esp8266/include
)

target_link_libraries(pico_fritzmon
  pico_stdlib
  pico_time
  pico_bootrom
  hardware_uart
  hardware_sleep
  hardware_rtc
  hardware_exception
  rp2040-esp-at
)

pico_enable_stdio_usb(pico_fritzmon 0)
pico_enable_stdio_uart(pico_fritzmon 0)

include(flash_ucode.cmake)

# create map/bin/hex file etc.
pico_add_extra_outputs(pico_fritzmon)

pico_set_program_url(pico_fritzmon
  "https://gitlab.com/slimhazard/pico-fritzmon"
)
pico_set_program_description(pico_fritzmon
  "Monitor a home network that has the TR-064 interface, such as AVM Fritz!Box"
)

set(PROGRAM_VERSION "unknown")
find_program(GIT git)
if (GIT)
  execute_process(COMMAND ${GIT} describe --always
    OUTPUT_VARIABLE GIT_DESCRIBE_OUTPUT
    OUTPUT_STRIP_TRAILING_WHITESPACE
    RESULT_VARIABLE GIT_DESCRIBE_RESULT
  )
  if (GIT_DESCRIBE_RESULT EQUAL "0")
    set(PROGRAM_VERSION ${GIT_DESCRIBE_OUTPUT})
  else()
    message(WARNING "git describe returned ${GIT_DESCRIBE_RESULT}")
  endif()
else()
  message(WARNING "git not found")
endif()
pico_set_program_version(pico_fritzmon ${PROGRAM_VERSION})
